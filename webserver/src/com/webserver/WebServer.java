package com.webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 用于监听指定端口上的http请求
 * 并调用线程进行处理
 * @author crazy
 *
 */
public class WebServer {

		/**
		 * 在指定端口进行监听
		 * @param port
		 */
		public void serverStart(int port){
			try {
				ServerSocket serverSocket = new ServerSocket(port);
				while(true){
					Socket socket = serverSocket.accept();
					new Processor(socket).start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public static void main(String[] args) {
			int port = 8088;
			if(args.length==1){
				port= Integer.parseInt(args[0]);
			}
			new WebServer().serverStart(port);
		}
}
