package com.webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * 用于处理http请求的线程
 * @author crazy
 *
 */
public class Processor extends Thread {

	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private final static String WEB_ROOT="/Users/crazy/work/staticfile/";
	
	public Processor(Socket socket)  {
		this.socket = socket;
		try {
			in = this.socket.getInputStream();
			out = new PrintStream(this.socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		String fileName = parse(in);
		sendFile(fileName);
	}
	
	/**
	 * 根据输入流获取想要请求的资源信息
	 * @param in
	 * @return
	 */
	public String parse(InputStream in){
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String fileName="";
		try {
			String httpMessage = br.readLine();
			String[] content = httpMessage.split(" ");
			//如果请求长度不为3 则返回错误信息
			if(content.length!=3){
				sendErrorMessage(400, "the http request was error!");
			}
			System.out.println("code:"+content[0]+",filename="+content[1]+",http version="+content[2]);
			fileName=content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return fileName;
	}
	
	/**
	 * 发送错误信息
	 * @param errorCode
	 * @param errorMessage
	 */
	public void sendErrorMessage(int errorCode,String errorMessage){
		out.println("HTTP/1.0 "+errorCode+" "+errorMessage);
		out.println("content-type: text/html");
		out.println();
		out.println("<html>");
		out.println("<title>Error Message");
		out.println("</title>");
		out.println("<body>");
		out.println("<h1>Errorcode:"+errorCode+",Message"+errorMessage);
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据请求的文件名，向浏览器端进行输出文件
	 * @param fileName
	 */
	public void sendFile(String fileName){
		File file = new File(WEB_ROOT+fileName);
		if(!file.exists()){
			sendErrorMessage(404, "File not found!~");
			return;
		}
		
		try {
			InputStream in = new FileInputStream(file);
			byte[] content = new byte[(int)file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 query file success");
			out.println("content-length:"+content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	
}
